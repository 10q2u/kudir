﻿namespace Kudir.Enums
{
    public enum OperationType
    {
        /// <summary>
        /// Доход
        /// </summary>
        Income,

        /// <summary>
        /// Расход
        /// </summary>
        Expenses
    }
}