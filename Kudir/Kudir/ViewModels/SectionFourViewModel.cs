﻿using GalaSoft.MvvmLight;

namespace Kudir.ViewModels
{
    public class SectionFourViewModel : ViewModelBase
    {
        #region Property

        #region 1й квартал

        #region PensionInsurancePremiumsFirstQuarter : decimal - страхое взносы на пенсионное страхование (1й квартал)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _pensionInsurancePremiumsFirstQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal PensionInsurancePremiumsFirstQuarter
        {
            get => _pensionInsurancePremiumsFirstQuarter;
            set => Set(ref _pensionInsurancePremiumsFirstQuarter, value);
        }

        #endregion

        #region HealthInsurancePremiumsFirstQuarter : decimal - страхое взносы на медицинское  страхование (1й квартал)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _healthInsurancePremiumsFirstQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal HealthInsurancePremiumsFirstQuarter
        {
            get => _healthInsurancePremiumsFirstQuarter;
            set => Set(ref _healthInsurancePremiumsFirstQuarter, value);
        }

        #endregion

        #region AmountFirstQuarter : decimal - Сумма за 1й квартал

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _amountFirstQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal AmountFirstQuarter
        {
            get => _amountFirstQuarter;
            set => Set(ref _amountFirstQuarter, value);
        }

        #endregion

        #endregion

        #region 2й квартал

        #region PensionInsurancePremiumsSecondQuarter : decimal - страхое взносы на пенсионное страхование (2й квартал)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _pensionInsurancePremiumsSecondQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal PensionInsurancePremiumsSecondQuarter
        {
            get => _pensionInsurancePremiumsSecondQuarter;
            set => Set(ref _pensionInsurancePremiumsSecondQuarter, value);
        }

        #endregion

        #region HealthInsurancePremiumsSecondQuarter : decimal - страхое взносы на медицинское страхование (2й квартал)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _healthInsurancePremiumsSecondQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal HealthInsurancePremiumsSecondQuarter
        {
            get => _healthInsurancePremiumsSecondQuarter;
            set => Set(ref _healthInsurancePremiumsSecondQuarter, value);
        }

        #endregion

        #region AmountSecondQuarter : decimal - Сумма за 2й квартал

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _amountSecondQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal AmountSecondQuarter
        {
            get => _amountSecondQuarter;
            set => Set(ref _amountSecondQuarter, value);
        }

        #endregion

        #region PensionInsurancePremiumsFourthQuarter : decimal - страхое взносы на пенсионное страхование (полугодие)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _pensionInsurancePremiumsFourthQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal PensionInsurancePremiumsFourthQuarter
        {
            get => _pensionInsurancePremiumsFourthQuarter;
            set => Set(ref _pensionInsurancePremiumsFourthQuarter, value);
        }

        #endregion

        #region HealthInsurancePremiumsFourthQuarter : decimal - страхое взносы на медицинское страхование (полугодие)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _healthInsurancePremiumsFourthQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal HealthInsurancePremiumsFourthQuarter
        {
            get => _healthInsurancePremiumsFourthQuarter;
            set => Set(ref _healthInsurancePremiumsFourthQuarter, value);
        }

        #endregion

        #region AmountFourthQuarter : decimal - Сумма за полугодие

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _amountFourthQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal AmountFourthQuarter
        {
            get => _amountFourthQuarter;
            set => Set(ref _amountFourthQuarter, value);
        }

        #endregion

        #endregion

        #region 3й квартал

        #region PensionInsurancePremiumsThirdQuarter : decimal - страхое взносы на пенсионное страхование (3й квартал)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _pensionInsurancePremiumsThirdQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal PensionInsurancePremiumsThirdQuarter
        {
            get => _pensionInsurancePremiumsThirdQuarter;
            set => Set(ref _pensionInsurancePremiumsThirdQuarter, value);
        }

        #endregion

        #region HealthInsurancePremiumsThirdQuarter : decimal - страхое взносы на медицинское страхование (3й квартал)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _healthInsurancePremiumsThirdQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal HealthInsurancePremiumsThirdQuarter
        {
            get => _healthInsurancePremiumsThirdQuarter;
            set => Set(ref _healthInsurancePremiumsThirdQuarter, value);
        }

        #endregion

        #region AmountThirdQuarter : decimal - Сумма за 3й квартал

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _amountThirdQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal AmountThirdQuarter
        {
            get => _amountThirdQuarter;
            set => Set(ref _amountThirdQuarter, value);
        }

        #endregion

        #region PensionInsurancePremiumsForNineMonths : decimal - страхое взносы на пенсионное страхование (за 9 месяцев)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _pensionInsurancePremiumsForNineMonths;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal PensionInsurancePremiumsForNineMonths
        {
            get => _pensionInsurancePremiumsForNineMonths;
            set => Set(ref _pensionInsurancePremiumsForNineMonths, value);
        }

        #endregion

        #region HealthInsurancePremiumsForNineMonths : decimal - страхое взносы на медицинское страхование (за 9 месяцев)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _healthInsurancePremiumsForNineMonths;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal HealthInsurancePremiumsForNineMonths
        {
            get => _healthInsurancePremiumsForNineMonths;
            set => Set(ref _healthInsurancePremiumsForNineMonths, value);
        }

        #endregion

        #region AmountForNineMonths : decimal - Сумма за 9 месяцев

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _amountForNineMonths;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal AmountForNineMonths
        {
            get => _amountForNineMonths;
            set => Set(ref _amountForNineMonths, value);
        }

        #endregion

        #endregion

        #region 4й квартал

        #region PensionInsurancePremiumsForHalfYearQuarter : decimal - страхое взносы на пенсионное страхование (4й квартал)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _pensionInsurancePremiumsForHalfYearQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal PensionInsurancePremiumsForHalfYearQuarter
        {
            get => _pensionInsurancePremiumsForHalfYearQuarter;
            set => Set(ref _pensionInsurancePremiumsForHalfYearQuarter, value);
        }

        #endregion

        #region HealthInsurancePremiumsForHalfYearQuarter : decimal - страхое взносы на медицинское страхование (4й квартал)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _healthInsurancePremiumsForHalfYearQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal HealthInsurancePremiumsForHalfYearQuarter
        {
            get => _healthInsurancePremiumsForHalfYearQuarter;
            set => Set(ref _healthInsurancePremiumsForHalfYearQuarter, value);
        }

        #endregion

        #region AmountForHalfYearQuarter : decimal - Сумма за 4й квартал

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _amountForHalfYearQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal AmountForHalfYearQuarter
        {
            get => _amountForHalfYearQuarter;
            set => Set(ref _amountForHalfYearQuarter, value);
        }

        #endregion

        #region PensionInsurancePremiumsTotalForYear : decimal - страхое взносы на пенсионное страхование (за год)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _pensionInsurancePremiumsTotalForYear;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal PensionInsurancePremiumsTotalForYear
        {
            get => _pensionInsurancePremiumsTotalForYear;
            set => Set(ref _pensionInsurancePremiumsTotalForYear, value);
        }

        #endregion

        #region HealthInsurancePremiumsTotalForYear : decimal - страхое взносы на медицинское страхование (за год)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _healthInsurancePremiumsTotalForYear;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal HealthInsurancePremiumsTotalForYear
        {
            get => _healthInsurancePremiumsTotalForYear;
            set => Set(ref _healthInsurancePremiumsTotalForYear, value);
        }

        #endregion

        #region AmountTotalForYear : decimal - Сумма за год

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _amountTotalForYear;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal AmountTotalForYear
        {
            get => _amountTotalForYear;
            set => Set(ref _amountTotalForYear, value);
        }

        #endregion

        #endregion

        #endregion
    }
}