using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using Kudir.Models;
using Kudir.Repositories.Interfaces;
using Kudir.Repositories.Services;
using Kudir.ViewModels.SectionOneVM;

// using Microsoft. .Practices.ServiceLocation;
namespace Kudir.ViewModels
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<IIncomeExpensesRepository, IncomeExpensesService>();

            // Window
            SimpleIoc.Default.Register<MainViewModel>();

            //UserControl
            SimpleIoc.Default.Register<TitleViewModel>(); //Lazy Loading
            SimpleIoc.Default.Register<FirstQuarterViewModel>(); //Eager Loading
            SimpleIoc.Default.Register<SecondQuarterViewModel>();
            SimpleIoc.Default.Register<ThirdQuarterViewModel>();
            SimpleIoc.Default.Register<ForHalfYearQuarterViewModel>();

            SimpleIoc.Default.Register<SectionFourViewModel>();
            SimpleIoc.Default.Register<SectionFiveViewModel>();
        }

        // System.Guid.NewGuid().ToString()
        public static MainViewModel MainWindowViewModel =>
            ServiceLocator.Current.GetInstance<MainViewModel>();

        public static TitleViewModel TitleViewModel =>
            ServiceLocator.Current.GetInstance<TitleViewModel>();

        #region 1й Раздел

        public static FirstQuarterViewModel FirstQuarterViewModel =>
            ServiceLocator.Current.GetInstance<FirstQuarterViewModel>();

        public static SecondQuarterViewModel SecondQuarterViewModel =>
            ServiceLocator.Current.GetInstance<SecondQuarterViewModel>();

        public static ThirdQuarterViewModel ThirdQuarterViewModel =>
            ServiceLocator.Current.GetInstance<ThirdQuarterViewModel>();

        public static ForHalfYearQuarterViewModel ForHalfYearQuarterViewModel =>
            ServiceLocator.Current.GetInstance<ForHalfYearQuarterViewModel>();

        #endregion

        public static SectionFourViewModel SectionFourViewModel =>
            ServiceLocator.Current.GetInstance<SectionFourViewModel>();

        public static SectionFiveViewModel SectionFiveViewModel =>
            ServiceLocator.Current.GetInstance<SectionFiveViewModel>();

        public static void Cleanup()
        {
        }
    }
}