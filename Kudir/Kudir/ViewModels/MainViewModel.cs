using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Kudir.Models;
using Kudir.Repositories.Interfaces;
using Kudir.ViewModels.SectionOneVM;

namespace Kudir.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private readonly IIncomeExpensesRepository _incomeExpensesRepository;

        private IncomeExpensesMessengerModel model;
        
        public MainViewModel(IIncomeExpensesRepository incomeExpensesRepository)
        {
            _incomeExpensesRepository = incomeExpensesRepository;
            CurrentModel = ViewModelLocator.TitleViewModel;
            
            Messenger.Default.Register<ChildViewModelState.Created>(this, "MainViewModel", state =>
            {
                Notify();
            });
            
            Messenger.Default.Register<ChildViewModelState.Closed>(this, "MainViewModel",state =>
            {
            });
        }

        #region Current UserControl

        #region CurrentModel : ViewModel - Текущая дочерняя модель-представления

        /// <summary>Текущая дочерняя модель-представления</summary>
        private ViewModelBase _currentModel;

        /// <summary>Текущая дочерняя модель-представления</summary>
        public ViewModelBase CurrentModel
        {
            get => _currentModel;
            private set => Set(ref _currentModel, value);
        }

        #endregion

        #endregion Current UserControl

        #region Property

        #region dateCreateReport : DateTime - Начало даты формирование отчета

        ///<summary>$DESCRIPTOIN</summary>$
        private DateTime _dateCreateReport = DateTime.Now;

        ///<summary>$DESCRIPTOIN</summary>$
        public DateTime DateCreateReport
        {
            get => _dateCreateReport;
            set => Set(ref _dateCreateReport, value);
        }

        #endregion

        #endregion

        #region Command

        #region Формирование отчета

        private void Notify()
        {
            Messenger.Default.Send(model);
        }
        
        public RelayCommand CreateReport => new RelayCommand((() =>
        {
            if (DateCreateReport >= DateTime.Now)
                return;
            model = new IncomeExpensesMessengerModel
            {
                IncomeExpensesModel = Seed.IncomeExpensesModels,
                DateTimeQuery = DateCreateReport
            };
            Notify();
        }));

        #endregion Формирование отчета

        #region Меню - переходы между отчетами

        #region OpenTitleReport : Command - команда для открытия формы титульного листа

        public RelayCommand OpenTitleReport => new RelayCommand((() =>
        {
            CurrentModel = ViewModelLocator.TitleViewModel;
        }));

        #endregion

        #region 1й Раздел

        #region ShowOpenFirstQuarterReport : Command - команда для открытия формы 1го квартал

        public RelayCommand OpenFirstQuarterReport => new RelayCommand((() =>
        {
            CurrentModel = ViewModelLocator.FirstQuarterViewModel;
        }));

        #endregion

        #region OpenSecondQuarterReport : Command - команда для открытия формы 2й квартал

        public RelayCommand OpenSecondQuarterReport => new RelayCommand((() =>
        {
            CurrentModel = ViewModelLocator.SecondQuarterViewModel;
        }));

        #endregion

        #region OpenThirdQuarterReport : Command - команда для открытия формы 3й квартал

        public RelayCommand OpenThirdQuarterReport => new RelayCommand((() =>
        {
            CurrentModel = ViewModelLocator.ThirdQuarterViewModel;
        }));

        #endregion

        #region OpenForHalfYearQuarterReport : Command - команда для открытия формы 4й квартал

        public RelayCommand OpenForHalfYearQuarterReport => new RelayCommand((() =>
        {
            CurrentModel = ViewModelLocator.ForHalfYearQuarterViewModel;
        }));

        #endregion

        #endregion

        #region Титульный лист

        public RelayCommand OpenTitleView =>
            new RelayCommand((() => { CurrentModel = ViewModelLocator.TitleViewModel; }));

        #endregion

        #region 4й Раздел

        public RelayCommand SectionFour =>
            new RelayCommand((() => { CurrentModel = ViewModelLocator.SectionFourViewModel; }));

        #endregion

        #region 5й Раздел

        public RelayCommand SectionFive =>
            new RelayCommand((() => { CurrentModel = ViewModelLocator.SectionFiveViewModel; }));

        #endregion

        #endregion Меню - переходы между отчетами

        #endregion Command
    }
}