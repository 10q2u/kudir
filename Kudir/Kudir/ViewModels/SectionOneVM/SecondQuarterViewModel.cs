﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Kudir.Models;
using Kudir.Repositories.Interfaces;

namespace Kudir.ViewModels.SectionOneVM
{
    public class SecondQuarterViewModel : ViewModelBase
    {
        private readonly IIncomeExpensesRepository _incomeExpensesRepository;
        private DateTime _dateTimeReportQuery;

        public SecondQuarterViewModel(IIncomeExpensesRepository incomeExpensesRepository)
        {
            _incomeExpensesRepository = incomeExpensesRepository;
            
            Messenger.Default.Register<IncomeExpensesMessengerModel>(this, Notify);
            Messenger.Default.Send(new ChildViewModelState.Created(this), "MainViewModel");
        }

        private void Notify(IncomeExpensesMessengerModel incomeExpensesMessenger)
        {
            Dispatcher.CurrentDispatcher.Invoke(() =>
            {
                _dateTimeReportQuery = incomeExpensesMessenger.DateTimeQuery;
                ItemsFistQuarter = new ObservableCollection<IncomeExpensesModel>(incomeExpensesMessenger
                    .IncomeExpensesModel
                    .Where(x =>
                        x.DateSales >= new DateTime(_dateTimeReportQuery.Year, 4, 1)
                        && x.DateSales <= new DateTime(_dateTimeReportQuery.Year, 6, 30)));

                ResultSecondQuarter = CalculationSecondQuarterIncome();
                ResultSecondQuarterExpenses = CalculationSecondQuarterExpenses();

                ResultFourthQuarterIncome = CalculationFourthQuarterIncomeResult();
                ResultFourthQuarterExpenses = CalculationFourthQuarterExpensesResult();
            });
        }

        #region Property

        #region ResultSecondQuarter : string - Результат за 2й квартал Доходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultSecondQuarterIncome;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultSecondQuarter
        {
            get => _resultSecondQuarterIncome;
            set => Set(ref _resultSecondQuarterIncome, value);
        }

        #endregion

        #region ResultSecondQuarterExpenses : string - Результат за 2й квартал Расходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultSecondQuarterExpenses;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultSecondQuarterExpenses
        {
            get => _resultSecondQuarterExpenses;
            set => Set(ref _resultSecondQuarterExpenses, value);
        }

        #endregion

        #region ResultFourthQuarterIncome : string - Результат за полугодие Доходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultFourthQuarterIncome;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultFourthQuarterIncome
        {
            get => _resultFourthQuarterIncome;
            set => Set(ref _resultFourthQuarterIncome, value);
        }

        #endregion

        #region ResultFourthQuarterExpenses : string - Результат за полугодие Расходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultFourthQuarterExpenses;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultFourthQuarterExpenses
        {
            get => _resultFourthQuarterExpenses;
            set => Set(ref _resultFourthQuarterExpenses, value);
        }

        #endregion

        #region IncomeExpensesModel : ObservableCollection - Коллекция

        /// <summary>DESCRIPTION</summary>
        private ObservableCollection<IncomeExpensesModel> _itemsFistQuarter;

        /// <summary>DESCRIPTION</summary>
        public ObservableCollection<IncomeExpensesModel> ItemsFistQuarter
        {
            get => _itemsFistQuarter;
            set
            {
                _itemsFistQuarter = value;
                RaisePropertyChanged();
            }
        }

        #endregion IncomeExpensesModel : ObservableCollection - Коллекция

        #endregion Property

        #region Итоги за 2й квартал

        private decimal CalculationSecondQuarterIncome()
        {
            if (ItemsFistQuarter == null)
                return 0;

            var result =
                _incomeExpensesRepository.CalculationSecondQuarterIncomeResult(ItemsFistQuarter,
                    _dateTimeReportQuery);
            return result;
        }

        private decimal CalculationSecondQuarterExpenses()
        {
            if (ItemsFistQuarter == null)
                return 0;

            var result =
                _incomeExpensesRepository.CalculationSecondQuarterExpensesResult(ItemsFistQuarter,
                    _dateTimeReportQuery);
            return result;
        }

        #endregion

        #region Итоги за пол года

        private decimal CalculationFourthQuarterIncomeResult()
        {
            if (ItemsFistQuarter == null)
                return 0;

            var result =
                _incomeExpensesRepository.CalculationFourthQuarterIncomeResult(ItemsFistQuarter,
                    _dateTimeReportQuery);
            return result;
        }

        private decimal CalculationFourthQuarterExpensesResult()
        {
            if (ItemsFistQuarter == null)
                return 0;

            var result =
                _incomeExpensesRepository.CalculationFourthQuarterExpensesResult(ItemsFistQuarter,
                    _dateTimeReportQuery);
            return result;
        }

        #endregion
    }
}