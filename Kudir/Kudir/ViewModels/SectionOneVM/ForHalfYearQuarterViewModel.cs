﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Kudir.Models;
using Kudir.Repositories.Interfaces;

namespace Kudir.ViewModels.SectionOneVM
{
    public class ForHalfYearQuarterViewModel : ViewModelBase
    {
        private readonly IIncomeExpensesRepository _incomeExpensesRepository;

        private DateTime _dateTimeReportQuery;

        public ForHalfYearQuarterViewModel(IIncomeExpensesRepository incomeExpensesRepository)
        {
            _incomeExpensesRepository = incomeExpensesRepository;
            Messenger.Default.Register<IncomeExpensesMessengerModel>(this, Notify);
            Messenger.Default.Send(new ChildViewModelState.Created(this), "MainViewModel");
        }

        private void Notify(IncomeExpensesMessengerModel incomeExpensesMessenger)
        {
            Dispatcher.CurrentDispatcher.Invoke(() =>
            {
                _dateTimeReportQuery = incomeExpensesMessenger.DateTimeQuery;
                ItemsFistQuarter = new ObservableCollection<IncomeExpensesModel>(incomeExpensesMessenger
                    .IncomeExpensesModel
                    .Where(x =>
                        x.DateSales >= new DateTime(_dateTimeReportQuery.Year, 10, 1)
                        && x.DateSales <= new DateTime(_dateTimeReportQuery.Year, 12, 31)));

                ResultForHalfYearQuarterIncome = CalculationSecondQuarterIncome();
                ResultForHalfYearQuarterExpenses = CalculationSecondQuarterExpenses();

                ResultTotalForYearIncome = CalculationTotalForYearIncomeResult();
                ResultTotalForYearExpenses = CalculationTotalForYearExpensesResult();
            });
        }

        #region Property

        #region ResultForHalfYearQuarterIncome : string - Результат за 4й квартал Доходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultForHalfYearQuarterIncome;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultForHalfYearQuarterIncome
        {
            get => _resultForHalfYearQuarterIncome;
            set => Set(ref _resultForHalfYearQuarterIncome, value);
        }

        #endregion

        #region ResultForHalfYearQuarterExpenses : string - Результат за 4й квартал Расходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultForHalfYearQuarterExpenses;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultForHalfYearQuarterExpenses
        {
            get => _resultForHalfYearQuarterExpenses;
            set => Set(ref _resultForHalfYearQuarterExpenses, value);
        }

        #endregion

        #region ResultTotalForYearIncome : string - Результат за год Доходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultTotalForYearIncome;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultTotalForYearIncome
        {
            get => _resultTotalForYearIncome;
            set => Set(ref _resultTotalForYearIncome, value);
        }

        #endregion

        #region ResultTotalForYearExpenses : string - Результат за год Расходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultTotalForYearExpenses;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultTotalForYearExpenses
        {
            get => _resultTotalForYearExpenses;
            set => Set(ref _resultTotalForYearExpenses, value);
        }

        #endregion

        #region IncomeExpensesModel : ObservableCollection - Коллекция

        /// <summary>DESCRIPTION</summary>
        private ObservableCollection<IncomeExpensesModel> _itemsFistQuarter;

        /// <summary>DESCRIPTION</summary>
        public ObservableCollection<IncomeExpensesModel> ItemsFistQuarter
        {
            get => _itemsFistQuarter;
            set
            {
                _itemsFistQuarter = value;
                RaisePropertyChanged();
            }
        }

        #endregion IncomeExpensesModel : ObservableCollection - Коллекция

        #endregion Property

        #region Итоги за 4й квартал

        private decimal CalculationSecondQuarterIncome()
        {
            if (ItemsFistQuarter == null)
                return 0;

            return _incomeExpensesRepository.CalculationForHalfYearIncomeResult(ItemsFistQuarter,
                _dateTimeReportQuery);
        }

        private decimal CalculationSecondQuarterExpenses()
        {
            if (ItemsFistQuarter == null)
                return 0;

            return _incomeExpensesRepository.CalculationForHalfYearExpensesResult(ItemsFistQuarter,
                _dateTimeReportQuery);
        }

        #endregion

        #region Итоги за год

        private decimal CalculationTotalForYearIncomeResult()
        {
            if (ItemsFistQuarter == null)
                return 0;

            return _incomeExpensesRepository.CalculationTotalForYearIncomeResult(ItemsFistQuarter,
                _dateTimeReportQuery);
        }

        private decimal CalculationTotalForYearExpensesResult()
        {
            if (ItemsFistQuarter == null)
                return 0;

            return _incomeExpensesRepository.CalculationTotalForYearExpensesResult(ItemsFistQuarter,
                _dateTimeReportQuery);
        }

        #endregion
    }
}