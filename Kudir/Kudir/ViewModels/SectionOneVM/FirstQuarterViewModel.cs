﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Kudir.Models;
using Kudir.Repositories.Interfaces;
using Kudir.Repositories.Services;

namespace Kudir.ViewModels.SectionOneVM
{
    public class FirstQuarterViewModel : ViewModelBase
    {
        private readonly IIncomeExpensesRepository _incomeExpensesRepository;
        private DateTime _dateTimeReportQuery;

        public FirstQuarterViewModel(IIncomeExpensesRepository incomeExpensesRepository)
        {
            _incomeExpensesRepository = incomeExpensesRepository;
            Messenger.Default.Register<IncomeExpensesMessengerModel>(this, Notify);
            Messenger.Default.Send(new ChildViewModelState.Created(this), "MainViewModel");
        }

        private void Notify(IncomeExpensesMessengerModel incomeExpensesMessenger)
        {
            _dateTimeReportQuery = incomeExpensesMessenger.DateTimeQuery;
            Dispatcher.CurrentDispatcher.Invoke(() =>
            {
                ItemsFistQuarter = new ObservableCollection<IncomeExpensesModel>(incomeExpensesMessenger
                    .IncomeExpensesModel
                    .Where(x =>
                        x.DateSales >= new DateTime(_dateTimeReportQuery.Year, 1, 1)
                        && x.DateSales <= new DateTime(_dateTimeReportQuery.Year, 3, 31)));
                ResultFirstQuarterIncome = CalculationFirstQuarterIncome();
                ResultFirstQuarterExpenses = CalculationFirstQuarterExpenses();
            });
        }

        #region Property

        #region ResultFirstQuarterIncome : string - Результат за 1й квартал Доходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultFirstQuarterIncome;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultFirstQuarterIncome
        {
            get => _resultFirstQuarterIncome;
            set => Set(ref _resultFirstQuarterIncome, value);
        }

        #endregion

        #region ResultFirstQuarterExpenses : string - Результат за 1й квартал Доходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultFirstQuarterExpenses;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultFirstQuarterExpenses
        {
            get => _resultFirstQuarterExpenses;
            set => Set(ref _resultFirstQuarterExpenses, value);
        }

        #endregion

        #region IncomeExpensesModel : ObservableCollection - Коллекция

        /// <summary>DESCRIPTION</summary>
        private ObservableCollection<IncomeExpensesModel> _itemsFistQuarter;

        /// <summary>DESCRIPTION</summary>
        public ObservableCollection<IncomeExpensesModel> ItemsFistQuarter
        {
            get => _itemsFistQuarter;
            set
            {
                _itemsFistQuarter = value;
                RaisePropertyChanged();
            }
        }

        #endregion IncomeExpensesModel : ObservableCollection - Коллекция

        #endregion Property

        #region Итоги за 1й квартал

        private decimal CalculationFirstQuarterIncome()
        {
            if (ItemsFistQuarter == null)
                return 0;

            var result =
                _incomeExpensesRepository.CalculationFirstQuarterIncomeResult(ItemsFistQuarter,
                    _dateTimeReportQuery);
            return result;
        }

        private decimal CalculationFirstQuarterExpenses()
        {
            if (ItemsFistQuarter == null)
                return 0;
            var result =
                _incomeExpensesRepository.CalculationFirstQuarterExpensesResult(ItemsFistQuarter,
                    _dateTimeReportQuery);
            return result;
        }

        #endregion
    }
}