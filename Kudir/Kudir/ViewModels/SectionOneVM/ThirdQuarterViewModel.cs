﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Kudir.Models;
using Kudir.Repositories.Interfaces;

namespace Kudir.ViewModels.SectionOneVM
{
    public class ThirdQuarterViewModel : ViewModelBase
    {
        private readonly IIncomeExpensesRepository _incomeExpensesRepository;
        private DateTime _dateTimeReportQuery;

        public ThirdQuarterViewModel(IIncomeExpensesRepository incomeExpensesRepository)
        {
            _incomeExpensesRepository = incomeExpensesRepository;

            Messenger.Default.Register<IncomeExpensesMessengerModel>(this, Notify);
            Messenger.Default.Send(new ChildViewModelState.Created(this), "MainViewModel");
        }

        private void Notify(IncomeExpensesMessengerModel incomeExpensesMessenger)
        {
            if (incomeExpensesMessenger == null)
                return;

            Dispatcher.CurrentDispatcher.Invoke(() =>
            {
                _dateTimeReportQuery = incomeExpensesMessenger.DateTimeQuery;
                ItemsFistQuarter = new ObservableCollection<IncomeExpensesModel>(incomeExpensesMessenger
                    .IncomeExpensesModel
                    .Where(x =>
                        x.DateSales >= new DateTime(_dateTimeReportQuery.Year, 7, 1)
                        && x.DateSales <= new DateTime(_dateTimeReportQuery.Year, 9, 30)));

                ResultThirdQuarterIncome = CalculationSecondQuarterIncome();
                ResultThirdQuarterExpenses = CalculationSecondQuarterExpenses();

                ResultForNineMonthsIncome = CalculationForNineMonthsIncomeResult();
                ResultForNineMonthsExpenses = CalculationForNineMonthsExpensesResult();
            });
        }

        #region Property

        #region ResultThirdQuarterIncome : string - Результат за 3й квартал Доходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultThirdQuarterIncome;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultThirdQuarterIncome
        {
            get => _resultThirdQuarterIncome;
            set => Set(ref _resultThirdQuarterIncome, value);
        }

        #endregion

        #region ResultSecondQuarterExpenses : string - Результат за 3й квартал Расходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultThirdQuarterExpenses;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultThirdQuarterExpenses
        {
            get => _resultThirdQuarterExpenses;
            set => Set(ref _resultThirdQuarterExpenses, value);
        }

        #endregion

        #region ResultForNineMonthsIncome : string - Результат за 9 месяцев Доходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultForNineMonthsIncome;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultForNineMonthsIncome
        {
            get => _resultForNineMonthsIncome;
            set => Set(ref _resultForNineMonthsIncome, value);
        }

        #endregion

        #region ResultForNineMonthsExpenses : string - Результат за полугодие Расходы

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _resultForNineMonthsExpenses;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal ResultForNineMonthsExpenses
        {
            get => _resultForNineMonthsExpenses;
            set => Set(ref _resultForNineMonthsExpenses, value);
        }

        #endregion

        #region IncomeExpensesModel : ObservableCollection - Коллекция

        /// <summary>DESCRIPTION</summary>
        private ObservableCollection<IncomeExpensesModel> _itemsFistQuarter;

        /// <summary>DESCRIPTION</summary>
        public ObservableCollection<IncomeExpensesModel> ItemsFistQuarter
        {
            get => _itemsFistQuarter;
            set
            {
                _itemsFistQuarter = value;
                RaisePropertyChanged();
            }
        }

        #endregion IncomeExpensesModel : ObservableCollection - Коллекция

        #endregion Property

        #region Итоги за 3й квартал

        private decimal CalculationSecondQuarterIncome()
        {
            if (ItemsFistQuarter == null)
                return 0;

            var result =
                _incomeExpensesRepository.CalculationThirdQuarterIncomeResult(ItemsFistQuarter,
                    _dateTimeReportQuery);
            return result;
        }

        private decimal CalculationSecondQuarterExpenses()
        {
            if (ItemsFistQuarter == null)
                return 0;

            var result =
                _incomeExpensesRepository.CalculationThirdQuarterExpensesResult(ItemsFistQuarter,
                    _dateTimeReportQuery);
            return result;
        }

        #endregion

        #region Итоги за 9 месяцев

        private decimal CalculationForNineMonthsIncomeResult()
        {
            if (ItemsFistQuarter == null)
                return 0;

            var result =
                _incomeExpensesRepository.CalculationForNineMonthsIncomeResult(ItemsFistQuarter,
                    _dateTimeReportQuery);
            return result;
        }

        private decimal CalculationForNineMonthsExpensesResult()
        {
            if (ItemsFistQuarter == null)
                return 0;

            var result =
                _incomeExpensesRepository.CalculationForNineMonthsExpensesResult(ItemsFistQuarter,
                    _dateTimeReportQuery);
            return result;
        }

        #endregion
    }
}