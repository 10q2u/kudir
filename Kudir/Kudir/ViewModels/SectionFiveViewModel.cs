﻿using GalaSoft.MvvmLight;

namespace Kudir.ViewModels
{
    public class SectionFiveViewModel : ViewModelBase
    {
        #region TradeFeePaidFirstQuarter : decimal - Сумма уплаченного торгового сбора (1й квартал)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _tradeFeePaidFirstQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal TradeFeePaidFirstQuarter
        {
            get => _tradeFeePaidFirstQuarter;
            set => Set(ref _tradeFeePaidFirstQuarter, value);
        }

        #endregion

        #region TradeFeePaidSecondQuarter : decimal - Сумма уплаченного торгового сбора (2й квартал)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _tradeFeePaidSecondQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal TradeFeePaidSecondQuarter
        {
            get => _tradeFeePaidSecondQuarter;
            set => Set(ref _tradeFeePaidSecondQuarter, value);
        }

        #endregion

        #region TradeFeePaidFourthQuarter : decimal - Сумма уплаченного торгового сбора (полугодие)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _tradeFeePaidFourthQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal TradeFeePaidFourthQuarter
        {
            get => _tradeFeePaidFourthQuarter;
            set => Set(ref _tradeFeePaidFourthQuarter, value);
        }

        #endregion


        #region TradeFeePaidThirdQuarter : decimal - Сумма уплаченного торгового сбора (3й квартал)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _tradeFeePaidThirdQuarter;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal TradeFeePaidThirdQuarter
        {
            get => _tradeFeePaidThirdQuarter;
            set => Set(ref _tradeFeePaidThirdQuarter, value);
        }

        #endregion

        #region TradeFeePaidForNineMonths : decimal - Сумма уплаченного торгового сбора (за 9 месяцев)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _tradeFeePaidForNineMonths;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal TradeFeePaidForNineMonths
        {
            get => _tradeFeePaidForNineMonths;
            set => Set(ref _tradeFeePaidForNineMonths, value);
        }

        #endregion


        #region TradeFeePaidForHalfYear : decimal - Сумма уплаченного торгового сбора (за 4й квартал)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _tradeFeePaidForHalfYear;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal TradeFeePaidForHalfYear
        {
            get => _tradeFeePaidForHalfYear;
            set => Set(ref _tradeFeePaidForHalfYear, value);
        }

        #endregion

        #region TradeFeePaidTotalForYear : decimal - Сумма уплаченного торгового сбора (за год)

        ///<summary>$DESCRIPTOIN</summary>$
        private decimal _tradeFeePaidTotalForYear;

        ///<summary>$DESCRIPTOIN</summary>$
        public decimal TradeFeePaidTotalForYear
        {
            get => _tradeFeePaidTotalForYear;
            set => Set(ref _tradeFeePaidTotalForYear, value);
        }

        #endregion
    }
}