﻿using System;
using GalaSoft.MvvmLight;
using Kudir.Enums;

namespace Kudir.ViewModels
{
    public class TitleViewModel : ViewModelBase
    {
        #region Property

        #region DateReport : DateTime - дата отчета

        private DateTime _dateReport;

        public DateTime DateReport
        {
            get => _dateReport;
            set => Set(ref _dateReport, value);
        }

        #endregion

        #region Taxpayer : string - Налогоплательщик

        private string _taxpayer;

        public string Taxpayer
        {
            get => _taxpayer;
            set => Set(ref _taxpayer, value);
        }

        #endregion Налогоплательщик

        #region Inn : string - ИНН

        private string _inn;

        public string Inn
        {
            get => _inn;
            set => Set(ref _inn, value);
        }

        #endregion ИНН

        #region Kpp : string - КПП

        private string _kpp;

        public string Kpp
        {
            get => _kpp;
            set => Set(ref _kpp, value);
        }

        #endregion КПП

        #region IdNumberTaxpayer : string - Идентификационный номер налогоплательщика

        private string _idNumberTaxpayer;

        public string IdNumberTaxpayer
        {
            get => _idNumberTaxpayer;
            set => Set(ref _idNumberTaxpayer, value);
        }

        #endregion Идентификационный номер налогоплательщика

        #region Taxation : string - Объект налогообложения

        private TaxationType _taxation;

        public TaxationType Taxation
        {
            get => _taxation;
            set => Set(ref _taxation, value);
        }

        #endregion Объект налогообложения

        #region Okei : long - ОКЕИ

        private long _okei;

        public long Okei
        {
            get => _okei;
            set => Set(ref _okei, value);
        }

        #endregion ОКЕИ

        #region Location : string - Адрес место нахождения организации

        private string _location;

        public string Location
        {
            get => _location;
            set => Set(ref _location, value);
        }

        #endregion Адрес место нахождения организации

        #region SettlementAccounts : long - Расчетные счета

        private long _settlementAccounts;

        public long SettlementAccounts
        {
            get => _settlementAccounts;
            set => Set(ref _settlementAccounts, value);
        }

        #endregion Расчетные счета

        #region NameSettlementAccounts : string - Название расчетные счета(Название банка)

        private string _nameSettlementAccounts;

        public string NameSettlementAccounts
        {
            get => _nameSettlementAccounts;
            set => Set(ref _nameSettlementAccounts, value);
        }

        #endregion Расчетные счета

        #endregion
    }
}