﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Kudir.Converters
{
    public class AbsoluteValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = value is decimal decValue ? Math.Abs(decValue) : throw new NotImplementedException();
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}