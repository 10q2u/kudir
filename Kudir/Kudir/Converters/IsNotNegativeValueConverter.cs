﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Kudir.Models;

namespace Kudir.Converters
{
    public class IsNotNegativeValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var returnValue = value is decimal ? (decimal) value > 0 : false;
            Debug.WriteLine($"Value {value} is {returnValue.ToString()}");
            return GetReturnValue(returnValue, targetType);
        }

        private static object GetReturnValue(bool value, Type targetType)
        {
            if (targetType == typeof(Visibility))
                return value ? Visibility.Visible : Visibility.Hidden;
            else
                return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}