﻿using System;
using System.Collections.Generic;
using Kudir.Enums;
using Kudir.Models;

namespace Kudir
{
    public static class Seed
    {
        public static readonly IEnumerable<IncomeExpensesModel> IncomeExpensesModels = new List<IncomeExpensesModel>
        {
            //За год -400 + 100 + 0 + 400 = 100
            //

            //Первый квартал = -400р
            new IncomeExpensesModel()
            {
                CheckNumber = 1, OperationType = OperationType.Income, DateSales = new DateTime(2020, 1, 10),
                IncomeExpenses = 100, OperationName = "Операция январь "
            },
            new IncomeExpensesModel()
            {
                CheckNumber = 2, OperationType = OperationType.Expenses, DateSales = new DateTime(2020, 2, 12),
                IncomeExpenses = -200, OperationName = "Операция февраль"
            },
            new IncomeExpensesModel()
            {
                CheckNumber = 3, OperationType = OperationType.Expenses, DateSales = new DateTime(2020, 3, 31),
                IncomeExpenses = -300, OperationName = "Операция март"
            },

            //Второй квартал = 100p
            new IncomeExpensesModel()
            {
                CheckNumber = 4, OperationType = OperationType.Income, DateSales = new DateTime(2020, 4, 13),
                IncomeExpenses = 100, OperationName = "Операция апрель "
            },
            new IncomeExpensesModel()
            {
                CheckNumber = 5, OperationType = OperationType.Expenses, DateSales = new DateTime(2020, 5, 11),
                IncomeExpenses = -200, OperationName = "Операция май"
            },
            new IncomeExpensesModel()
            {
                CheckNumber = 6, OperationType = OperationType.Income, DateSales = new DateTime(2020, 6, 30),
                IncomeExpenses = 200, OperationName = "Операция июнь"
            },

            //Третий квартал = 0p
            new IncomeExpensesModel()
            {
                CheckNumber = 7, OperationType = OperationType.Expenses, DateSales = new DateTime(2020, 7, 18),
                IncomeExpenses = -300, OperationName = "Операция  июль"
            },
            new IncomeExpensesModel()
            {
                CheckNumber = 8, OperationType = OperationType.Expenses, DateSales = new DateTime(2020, 8, 31),
                IncomeExpenses = -200, OperationName = "Операция август"
            },
            new IncomeExpensesModel()
            {
                CheckNumber = 9, OperationType = OperationType.Income, DateSales = new DateTime(2020, 9, 21),
                IncomeExpenses = 500, OperationName = "Операция сентябрь"
            },

            //Четвертый квартал = 400p
            new IncomeExpensesModel()
            {
                CheckNumber = 10, OperationType = OperationType.Expenses, DateSales = new DateTime(2020, 10, 22),
                IncomeExpenses = -300, OperationName = "Операция  октябрь"
            },
            new IncomeExpensesModel()
            {
                CheckNumber = 11, OperationType = OperationType.Income, DateSales = new DateTime(2020, 11, 10),
                IncomeExpenses = 200, OperationName = "Операция ноябрь"
            },
            new IncomeExpensesModel()
            {
                CheckNumber = 12, OperationType = OperationType.Income, DateSales = new DateTime(2020, 12, 8),
                IncomeExpenses = 500, OperationName = "Операция декабрь"
            },
        };
    }
}