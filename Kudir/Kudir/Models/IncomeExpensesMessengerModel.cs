﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Kudir.Annotations;

namespace Kudir.Models
{
    public class IncomeExpensesMessengerModel : INotifyPropertyChanged
    {
        private IEnumerable<IncomeExpensesModel> _incomeExpensesModel;

        public IEnumerable<IncomeExpensesModel> IncomeExpensesModel
        {
            get => _incomeExpensesModel;
            set
            {
                _incomeExpensesModel = value;
                OnPropertyChanged("IncomeExpensesModel");
            }
        }

        private DateTime _dateTimeQuery;

        public DateTime DateTimeQuery
        {
            get => _dateTimeQuery;
            set
            {
                _dateTimeQuery = value;
                OnPropertyChanged("DateTimeQuery");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}