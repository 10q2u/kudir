﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Kudir.Annotations;
using Kudir.Enums;

namespace Kudir.Models
{
    public class TitlePageModel : INotifyPropertyChanged
    {
        #region DateReport - дата отчета

        private DateTime _dateReport;

        public DateTime DateReport
        {
            get => _dateReport;
            set
            {
                _dateReport = value;
                OnPropertyChanged("DateReport");
            }
        }

        #endregion

        #region Taxpayer - Налогоплательщик

        private string _taxpayer;

        public string Taxpayer
        {
            get => _taxpayer;
            set
            {
                _taxpayer = value;
                OnPropertyChanged("Taxpayer");
            }
        }

        #endregion Налогоплательщик

        #region Inn - ИНН

        private string _inn;

        public string Inn
        {
            get => _inn;
            set
            {
                _inn = value;
                OnPropertyChanged("Inn");
            }
        }

        #endregion ИНН

        #region Kpp - КПП

        private string _kpp;

        public string Kpp
        {
            get => _kpp;
            set
            {
                _kpp = value;
                OnPropertyChanged("Kpp");
            }
        }

        #endregion КПП

        #region IdNumberTaxpayer - Идентификационный номер налогоплательщика

        private string _idNumberTaxpayer;

        public string IdNumberTaxpayer
        {
            get => _idNumberTaxpayer;
            set
            {
                _idNumberTaxpayer = value;
                OnPropertyChanged("IdNumberTaxpayer");
            }
        }

        #endregion Идентификационный номер налогоплательщика

        #region Taxation - Объект налогообложения

        private TaxationType _taxation;

        public TaxationType Taxation
        {
            get => _taxation;
            set
            {
                _taxation = value;
                OnPropertyChanged("Taxation");
            }
        }

        #endregion Объект налогообложения

        #region Okei - ОКЕИ

        private long _okei;

        public long Okei
        {
            get => _okei;
            set
            {
                _okei = value;
                OnPropertyChanged("Okei");
            }
        }

        #endregion ОКЕИ

        #region Location - Адрес место нахождения организации

        private string _location;

        public string Location
        {
            get => _location;
            set
            {
                _location = value;
                OnPropertyChanged("Location");
            }
        }

        #endregion Адрес место нахождения организации

        #region SettlementAccounts - Расчетные счета

        private long _settlementAccounts;

        public long SettlementAccounts
        {
            get => _settlementAccounts;
            set
            {
                _settlementAccounts = value;
                OnPropertyChanged("SettlementAccounts");
            }
        }

        #endregion Расчетные счета

        #region NameSettlementAccounts - Название расчетные счета(Название банка)

        private string _nameSettlementAccounts;

        public string NameSettlementAccounts
        {
            get => _nameSettlementAccounts;
            set
            {
                _nameSettlementAccounts = value;
                OnPropertyChanged("NameSettlementAccounts");
            }
        }

        #endregion Расчетные счета

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}