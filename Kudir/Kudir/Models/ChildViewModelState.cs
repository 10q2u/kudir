﻿using GalaSoft.MvvmLight;

namespace Kudir.Models
{
    public  class ChildViewModelState
    {
        public class Created : ChildViewModelState
        {
            public ViewModelBase Child { get; }

            public Created(ViewModelBase child)
            {
                this.Child = child;
            }
        }

        public class Closed : ChildViewModelState
        {
            
        }
    }
}