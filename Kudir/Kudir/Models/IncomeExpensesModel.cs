﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Kudir.Annotations;
using Kudir.Enums;

namespace Kudir.Models
{
    public class IncomeExpensesModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Дата осуществления продажи
        /// </summary>
        private DateTime _dateTime;

        public DateTime DateSales
        {
            get => _dateTime;
            set
            {
                _dateTime = value;
                OnPropertyChanged("DateSales");
            }
        }

        /// <summary>
        /// Тип операции
        /// </summary>
        private OperationType _operationType;

        public OperationType OperationType
        {
            get => _operationType;
            set
            {
                _operationType = value;
                OnPropertyChanged("OperationType");
            }
        }

        /// <summary>
        /// Номер первичного документа
        /// </summary>
        private long _checkNumber;

        public long CheckNumber
        {
            get => _checkNumber;
            set
            {
                _checkNumber = value;
                OnPropertyChanged("CheckNumber");
            }
        }

        /// <summary>
        /// Название операции (Содержание операции)
        /// </summary>
        private string _operationName;

        public string OperationName
        {
            get => _operationName;
            set
            {
                _operationName = value;
                OnPropertyChanged("OperationName");
            }
        }

        /// <summary>
        /// Доходы или расходы при исчисоении налоговой базы(Сумма)
        /// </summary>
        private decimal _incomeExpenses;

        public decimal IncomeExpenses
        {
            get => _incomeExpenses;
            set
            {
                _incomeExpenses = value;
                OnPropertyChanged("IncomeExpenses");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}