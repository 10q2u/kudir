﻿using System;
using System.Collections.Generic;
using Kudir.Models;

namespace Kudir.Repositories.Interfaces
{
    public interface IIncomeExpensesRepository
    {
        // #region Итоги
        //
        // /// <summary>
        // /// Итоги за первый квартал
        // /// </summary>
        // decimal FirstQuarterResult { get; set; }
        //
        // /// <summary>
        // /// Итого за второй квартал																																																						
        // /// </summary>
        // decimal SecondQuarterResult { get; set; }
        //
        // /// <summary>
        // /// Итоги за третьего квартал
        // /// </summary>
        // decimal ThirdQuarterResult { get; set; }
        //
        // /// <summary>
        // /// Итоги за четвертого квартал
        // /// </summary>
        // decimal FourthQuarterResult { get; set; }
        //
        // /// <summary>
        // /// Итоги за пол года
        // /// </summary>
        // decimal ResultForHalfYear { get; set; }
        //
        // /// <summary>
        // /// Итого за 9 месяцев																																																									
        // /// </summary>
        // decimal ResultsForNineMonths { get; set; }
        //
        // /// <summary>
        // /// Итоги за год
        // /// </summary>
        // decimal TotalForYear { get; set; }
        //
        // #endregion Итоги

        #region Методы

        #region FirstQuarterResult - Общие, Доходы, Расходы

        /// <summary>
        /// Расчет итогов за первый квартал 
        /// </summary>
        /// <returns></returns>
        decimal CalculationFirstQuarterResult(IEnumerable<IncomeExpensesModel> incomeExpenses, DateTime queryYear);

        /// <summary>
        /// Расчет итогов за первый квартал Доходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationFirstQuarterIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        /// <summary>
        /// Расчет итогов за первый квартал Расходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationFirstQuarterExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        #endregion FirstQuarterResult - Общие, Доходы, Расходы

        #region SecondQuarterResult - Общие, Доходы, Расходы

        /// <summary>
        /// Расчет итогов за второй квартал 
        /// </summary>
        /// <returns></returns>
        decimal CalculationSecondQuarterResult(IEnumerable<IncomeExpensesModel> incomeExpenses, DateTime queryYear);

        /// <summary>
        /// Расчет итогов за второй квартал Доходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationSecondQuarterIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        /// <summary>
        /// Расчет итогов за второй квартал Расходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationSecondQuarterExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        #endregion SecondQuarterResult  - Общие, Доходы, Расходы

        #region ThirdQuarterResult - Общие, Доходы, Расходы

        /// <summary>
        /// Расчет итогов за третий квартал 
        /// </summary>
        /// <returns></returns>
        decimal CalculationThirdQuarterResult(IEnumerable<IncomeExpensesModel> incomeExpenses, DateTime queryYear);

        /// <summary>
        /// Расчет итогов за третий квартал Доходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationThirdQuarterIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        /// <summary>
        /// Расчет итогов за третий квартал Расходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationThirdQuarterExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        #endregion ThirdQuarterResult  - Общие, Доходы, Расходы

        #region ResultForHalfYear - Общие, Доходы, Расходы

        /// <summary>
        /// Расчет итогов за четвертый квартал 
        /// </summary>
        /// <returns></returns>
        decimal CalculationResultForHalfYear(IEnumerable<IncomeExpensesModel> incomeExpenses, DateTime queryYear);

        /// <summary>
        /// Расчет итогов за четвертый квартал Доходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationForHalfYearIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        /// <summary>
        /// Расчет итогов за четвертый квартал Расходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationForHalfYearExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        #endregion ResultForHalfYear - Общие, Доходы, Расходы


        #region FourthQuarterResult - Итого за пол года (Общие, Доходы, Расходы)

        /// <summary>
        /// Расчет итогов за пол года
        /// </summary>
        /// <returns></returns>
        decimal CalculationFourthQuarterResult(IEnumerable<IncomeExpensesModel> incomeExpenses, DateTime queryYear);

        /// <summary>
        /// Расчет итогов за пол года - Доходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationFourthQuarterIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        /// <summary>
        /// Расчет итогов за пол года - Расходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationFourthQuarterExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        #endregion FourthQuarterResult - Общие, Доходы, Расходы

        #region ResultsForNineMonths - Итогов за 9 месяцев (Общие, Доходы, Расходы)

        /// <summary>
        /// Расчет итогов за 9 месяцев		
        /// </summary>
        /// <returns></returns>
        decimal CalculationResultsForNineMonths(IEnumerable<IncomeExpensesModel> incomeExpenses, DateTime queryYear);

        /// <summary>
        /// Расчет итогов за пол года Доходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationForNineMonthsIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        /// <summary>
        /// Расчет итогов за пол года - Расходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationForNineMonthsExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        #endregion

        #region TotalForYear - Итоги за год (Общие, Доходы, Расходы)

        /// <summary>
        /// Расчет итогов за год
        /// </summary>
        /// <returns></returns>
        decimal CalculationTotalForYear(IEnumerable<IncomeExpensesModel> incomeExpenses, DateTime queryYear);

        /// <summary>
        /// Расчет итогов за год Доходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationTotalForYearIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        /// <summary>
        /// Расчет итогов за год - Расходы
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <param name="queryYear"></param>
        /// <returns></returns>
        decimal CalculationTotalForYearExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear);

        #endregion


        /// <summary>
        /// Расчет Доходов
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <returns></returns>
        decimal CalculationTotalIncome(IEnumerable<IncomeExpensesModel> incomeExpenses, DateTime queryYear);

        /// <summary>
        /// Расчет Расходов
        /// </summary>
        /// <param name="incomeExpenses"></param>
        /// <returns></returns>
        decimal CalculationTotalExpenses(IEnumerable<IncomeExpensesModel> incomeExpenses, DateTime queryYear);

        #endregion Методы
    }
}