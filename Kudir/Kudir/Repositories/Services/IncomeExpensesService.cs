﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kudir.Enums;
using Kudir.Models;
using Kudir.Repositories.Interfaces;

namespace Kudir.Repositories.Services
{
    public class IncomeExpensesService : IIncomeExpensesRepository
    {
        #region FirstQuarterResult - Общие, Доходы, Расходы

        public decimal CalculationFirstQuarterResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var firstQuarterResult = incomeExpenses
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 1, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 3, 31))
                .Sum(x => x.IncomeExpenses);
            return firstQuarterResult;
        }

        public decimal CalculationFirstQuarterIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var firstQuarterResult = incomeExpenses
                .Where(x => x.OperationType == OperationType.Income)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 1, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 3, 31))
                .Sum(x => x.IncomeExpenses);
            return firstQuarterResult;
        }

        public decimal CalculationFirstQuarterExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var firstQuarterResult = incomeExpenses
                .Where(x => x.OperationType == OperationType.Expenses)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 1, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 3, 31))
                .Sum(x => x.IncomeExpenses);
            return firstQuarterResult;
        }

        #endregion FirstQuarterResult - Общие, Доходы, Расходы

        #region SecondQuarterResult - Общие, Доходы, Расходы

        public decimal CalculationSecondQuarterResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var secondQuarterResult = incomeExpenses
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 4, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 6, 30))
                .Sum(x => x.IncomeExpenses);
            return secondQuarterResult;
        }

        public decimal CalculationSecondQuarterIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var firstQuarterResult = incomeExpenses
                .Where(x => x.OperationType == OperationType.Income)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 4, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 6, 30))
                .Sum(x => x.IncomeExpenses);
            return firstQuarterResult;
        }

        public decimal CalculationSecondQuarterExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var firstQuarterResult = incomeExpenses
                .Where(x => x.OperationType == OperationType.Expenses)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 4, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 6, 30))
                .Sum(x => x.IncomeExpenses);
            return firstQuarterResult;
        }

        #endregion SecondQuarterResult - Общие, Доходы, Расходы

        #region ThirdQuarterResult - Общие, Доходы, Расходы

        public decimal CalculationThirdQuarterResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var thirdQuarterResult = incomeExpenses
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 7, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 9, 30))
                .Sum(x => x.IncomeExpenses);
            return thirdQuarterResult;
        }

        public decimal CalculationThirdQuarterIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var thirdQuarterResult = incomeExpenses
                .Where(x => x.OperationType == OperationType.Income)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 7, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 9, 30))
                .Sum(x => x.IncomeExpenses);
            return thirdQuarterResult;
        }

        public decimal CalculationThirdQuarterExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var thirdQuarterResult = incomeExpenses
                .Where(x => x.OperationType == OperationType.Expenses)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 7, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 9, 30))
                .Sum(x => x.IncomeExpenses);
            return thirdQuarterResult;
        }

        #endregion ThirdQuarterResult - Общие, Доходы, Расходы

        #region ResultForHalfYear - Общие, Доходы, Расходы

        public decimal CalculationResultForHalfYear(IEnumerable<IncomeExpensesModel> incomeExpenses, DateTime queryYear)
        {
            var resultForHalfYear = incomeExpenses
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 10, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 12, 31))
                .Sum(x => x.IncomeExpenses);
            return resultForHalfYear;
        }

        public decimal CalculationForHalfYearIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var resultForHalfYear = incomeExpenses
                .Where(x => x.OperationType == OperationType.Income)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 10, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 12, 31))
                .Sum(x => x.IncomeExpenses);
            return resultForHalfYear;
        }

        public decimal CalculationForHalfYearExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var resultForHalfYear = incomeExpenses
                .Where(x => x.OperationType == OperationType.Expenses)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 10, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 12, 31))
                .Sum(x => x.IncomeExpenses);
            return resultForHalfYear;
        }

        #endregion ResultForHalfYear - Общие, Доходы, Расходы

        #region FourthQuarterResult - Расчет итогов за пол года

        public decimal CalculationFourthQuarterResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var fourthQuarterResult = incomeExpenses
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 1, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 6, 30))
                .Sum(x => x.IncomeExpenses);
            return fourthQuarterResult;
        }

        public decimal CalculationFourthQuarterIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var fourthQuarterResult = incomeExpenses
                .Where(x => x.OperationType == OperationType.Income)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 1, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 6, 30))
                .Sum(x => x.IncomeExpenses);
            return fourthQuarterResult;
        }

        public decimal CalculationFourthQuarterExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var fourthQuarterResult = incomeExpenses
                .Where(x => x.OperationType == OperationType.Expenses)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 1, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 6, 30))
                .Sum(x => x.IncomeExpenses);
            return fourthQuarterResult;
        }

        #endregion

        #region ResultsForNineMonths - Расчет итого за 9 месяцев

        public decimal CalculationResultsForNineMonths(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var resultsForNineMonths = incomeExpenses
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 1, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 9, 30))
                .Sum(x => x.IncomeExpenses);
            return resultsForNineMonths;
        }

        public decimal CalculationForNineMonthsIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var resultsForNineMonths = incomeExpenses
                .Where(x => x.OperationType == OperationType.Income)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 1, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 9, 30))
                .Sum(x => x.IncomeExpenses);
            return resultsForNineMonths;
        }

        public decimal CalculationForNineMonthsExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var resultsForNineMonths = incomeExpenses
                .Where(x => x.OperationType == OperationType.Expenses)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 1, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 9, 30))
                .Sum(x => x.IncomeExpenses);
            return resultsForNineMonths;
        }

        #endregion

        #region TotalForYearResult - Расчет итого за 9 месяцев

        public decimal CalculationTotalForYear(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var totalForYear = incomeExpenses
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 1, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 12, 31))
                .Sum(x => x.IncomeExpenses);
            return totalForYear;
        }

        public decimal CalculationTotalForYearIncomeResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var totalForYear = incomeExpenses
                .Where(x => x.OperationType == OperationType.Income)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 1, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 12, 31))
                .Sum(x => x.IncomeExpenses);
            return totalForYear;
        }

        public decimal CalculationTotalForYearExpensesResult(IEnumerable<IncomeExpensesModel> incomeExpenses,
            DateTime queryYear)
        {
            var totalForYear = incomeExpenses
                .Where(x => x.OperationType == OperationType.Expenses)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Where(x =>
                    x.DateSales >= new DateTime(queryYear.Year, 1, 1)
                    && x.DateSales <= new DateTime(queryYear.Year, 12, 31))
                .Sum(x => x.IncomeExpenses);
            return totalForYear;
        }

        #endregion

        public decimal CalculationTotalIncome(IEnumerable<IncomeExpensesModel> incomeExpenses, DateTime queryYear)
        {
            var resultTotalIncome = incomeExpenses
                .Where(x => x.OperationType == OperationType.Income)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Sum(x => x.IncomeExpenses);
            return resultTotalIncome;
        }

        public decimal CalculationTotalExpenses(IEnumerable<IncomeExpensesModel> incomeExpenses, DateTime queryYear)
        {
            var resultTotalExpenses = incomeExpenses
                .Where(x => x.OperationType == OperationType.Expenses)
                .Where(x => x.DateSales.Year == queryYear.Year)
                .Sum(x => x.IncomeExpenses);
            return resultTotalExpenses;
        }
    }
}